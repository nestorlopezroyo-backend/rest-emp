package com.techuniversity.emp.utils;

import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class UtilidadesTest {

    public void testGetCadena() throws BadSeparator {
        String result = Utilidades.getCadena("Enric Bala",".");
        assertEquals("E.N.R.I.C B.A.L.A", result);
    }
}
