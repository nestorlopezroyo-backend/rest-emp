package com.techuniversity.emp.controller;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.EstadosPedido;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testCadena(){
        String result = empleadosController.cadena("Enric Bala",".");
        assertEquals("E.N.R.I.C B.A.L.A", result);
    }

    @Test
    public void testBadSeparator(){
        try{
            String result = Utilidades.getCadena("Enric Bala","..");
            fail("Se esperaba BadSeparator Exception.");
        } catch (BadSeparator bs){

        }

    }

    @ParameterizedTest
    @ValueSource(ints = {1,3,5,-3,15, Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    public void testEstaBlancoNull(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    public void testEstaBlancoNullEmpty(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }
    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testEstadosPedido(EstadosPedido estado){
        assertTrue(Utilidades.valorarEstadoPedido(estado));
    }
}
