package com.techuniversity.emp.repositorios;

import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.model.Empleados;
import com.techuniversity.emp.model.Formacion;
import org.springframework.stereotype.Repository;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadosDAO {
    private static Empleados lista = new Empleados();

    static {
        Formacion form1 = new Formacion("2021-05-18","Curso Back");
        Formacion form2 = new Formacion("2020-03-08","Curso Aso");
        Formacion form3 = new Formacion("2005-08-01","Curso Mainframe");
        ArrayList<Formacion> una = new ArrayList<Formacion>();
        una.add(form1);
        ArrayList<Formacion> dos = new ArrayList<Formacion>();
        dos.add(form1);
        dos.add(form2);
        ArrayList<Formacion> tres = new ArrayList<Formacion>();
        tres.add(form1);
        tres.add(form2);
        tres.add(form3);
        ArrayList<Formacion> vacio = new ArrayList<Formacion>();

        lista.getListaEmpleados().add(new Empleado(1, "Néstor","López","nlopez@techuniversiti.com", una));

        lista.getListaEmpleados().add(new Empleado(2, "Ruben","Arregui","rarregui@techuniversiti.com", dos));

        lista.getListaEmpleados().add(new Empleado(3, "Victor","Gil","vgil@techuniversiti.com", tres));

        lista.getListaEmpleados().add(new Empleado(4, "Oscar","Llamas","ollamas@techuniversiti.com", vacio));
    }

    public Empleados getAllEmpleados(){
        return lista;
    }

    public Empleado getEmpleadoById(int id){
        for (Empleado emp :lista.getListaEmpleados()){
            if(emp.getId() == id) {
                return emp;
            }
        }
        return null;
    }

    public void addEmpleado(Empleado emp){
        lista.getListaEmpleados().add(emp);
    }

    public Empleado updEmpleado(Empleado emp) {
        Empleado currentEmp = getEmpleadoById(emp.getId());
        if (currentEmp != null) {
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());
        }
        return currentEmp;
    }

    public Empleado updEmpleado(int id, Empleado emp) {
        Empleado currentEmp = getEmpleadoById(id);
        if (currentEmp != null) {
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());
        }
        return currentEmp;
    }

    public void delEmpleado(int id){
        Iterator itr = lista.getListaEmpleados().iterator(); //Obtengo iterador.

        while(itr.hasNext()){
            Empleado emp = (Empleado) itr.next(); // Forzamos cast al tipo de objeto
            if(emp.getId() == id) {
                itr.remove();
                break;
            }
        }
    }

    public Empleado softUpdEmpleado(int id, Map<String, Object> updates){
        Empleado currentEmp = getEmpleadoById(id);
        if(currentEmp != null) {
            for (Map.Entry<String, Object> update:updates.entrySet()){
                switch(update.getKey()){
                    case "nombre":
                        currentEmp.setNombre(update.getValue().toString());
                        break;
                    case "apellidos":
                        currentEmp.setApellidos(update.getValue().toString());
                        break;
                    case "email":
                        currentEmp.setEmail(update.getValue().toString());
                        break;
                }
            }
        }
        return currentEmp;
    }

    public List<Formacion> getFormacionEmpleado(int id){
        Empleado emp = getEmpleadoById(id);
        if(emp != null){
            return getEmpleadoById(id).getFormaciones();
        }else{
            return null;
        }
    }

    public Empleado addFormacionEmpleado(int id, Formacion formacion){
        Empleado emp = getEmpleadoById(id);
        if(emp != null){
            emp.getFormaciones().add(formacion);
        }
        return emp;
    }
}
