package com.techuniversity.emp.utils;

public enum EstadosPedido {
    ACEPTADO,
    COCINADO,
    EN_REPARTO,
    ENTREGADO,
    VALORADO;
}
