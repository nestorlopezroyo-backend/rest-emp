package com.techuniversity.emp.controllers;

import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.model.Empleados;
import com.techuniversity.emp.model.Formacion;
import com.techuniversity.emp.repositorios.EmpleadosDAO;
import com.techuniversity.emp.utils.Configuracion;
import com.techuniversity.emp.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadosDAO empleadosDAO;

    @GetMapping(path = "/", produces = "application/json")
    public Empleados getEmpleados() {
        return empleadosDAO.getAllEmpleados();
    }


    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id){
        Empleado emp = empleadosDAO.getEmpleadoById(id);
        if (emp != null) return new ResponseEntity<>(emp, HttpStatus.OK);
        else return ResponseEntity.notFound().build();

    }

    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        Integer id = empleadosDAO.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setId(id);
        empleadosDAO.addEmpleado(emp);
        // Utilidad para crear URI.
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();

        return ResponseEntity.created(location).build();//201
    }


    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp){
        Empleado modEmpleado = empleadosDAO.updEmpleado(emp);
        if(modEmpleado == null) {
            return ResponseEntity.notFound().build();//404
        }else{
            return ResponseEntity.ok().build();//200
        }
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id, @RequestBody Empleado emp){
        Empleado modEmpleado = empleadosDAO.updEmpleado(id, emp);
        if(modEmpleado == null) {
            return ResponseEntity.notFound().build(); //404
        }else{
            return ResponseEntity.ok().build();//200
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleado(@PathVariable int id){
        empleadosDAO.delEmpleado(id);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> patchEmpleado(@PathVariable int id, @RequestBody Map<String,Object> updates){
        Empleado emp = empleadosDAO.softUpdEmpleado(id,updates);
        if(emp == null) {
            return ResponseEntity.notFound().build(); //404
        }else{
            return ResponseEntity.ok().build(); //200
        }
    }

    @GetMapping(path = "/{id}/formaciones")
    public ResponseEntity<List<Formacion>> getFormacionesEmpleado(@PathVariable int id){
        List<Formacion> formaciones = empleadosDAO.getFormacionEmpleado(id);
        if (formaciones == null) {
            return ResponseEntity.notFound().build(); //404
        }else{
            //return new ResponseEntity<List<Formacion>>( formaciones, HttpStatus.OK);
            return ResponseEntity.ok().body(formaciones);
        }

    }

    @PostMapping(path = "/{id}/formaciones/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addFormacionDeEmpleado(@PathVariable int id, @RequestBody Formacion formacion) {
        Empleado emp = empleadosDAO.addFormacionEmpleado(id, formacion);
        if(emp == null){
            return ResponseEntity.notFound().build();//404
        }
        else{
            return ResponseEntity.ok().build();//200
        }
    }

    @Value("${app.titulo}") private String titulo; //Inyecta el valor de app.titulo a la variable titulo.

    @GetMapping("/titulo")
    public String getTitulo(){
        String modo = config.getModo();
        return titulo + " " + modo ;
    }

    @Autowired
    private Environment environment;

    @GetMapping("/prop")
    public String getPropiedad(@RequestParam("key") String key ){
        String valor = "Sin valor";
        String keyValor = environment.getProperty(key);
        if((keyValor != null) && !keyValor.isEmpty()){
            valor = keyValor;
        }
        return valor;
    }

    @Autowired
    private Configuracion config;

    @GetMapping("/autor")
    public String getAutor(){
        return config.getAutor();
    }

    @GetMapping("/home")
    public String home(){
        return "Don't worry";
    }

    @GetMapping("/cadena")
    public String cadena(@RequestParam String texto, @RequestParam String separador){
        try{
            return Utilidades.getCadena(texto, separador);
        }
        catch (Exception ex){
            return ex.getMessage();
        }
    }
}
